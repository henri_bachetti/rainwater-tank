EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4xxx:4011 U1
U 1 1 62542B25
P 5150 1950
F 0 "U1" H 5100 2150 50  0000 C CNN
F 1 "4011" H 5150 1950 50  0000 C CNN
F 2 "" H 5150 1950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 5150 1950 50  0001 C CNN
	1    5150 1950
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4011 U1
U 2 1 625984EE
P 5150 2500
F 0 "U1" H 5100 2300 50  0000 C CNN
F 1 "4011" H 5150 2500 50  0000 C CNN
F 2 "" H 5150 2500 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 5150 2500 50  0001 C CNN
	2    5150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 1950 5550 1950
Wire Wire Line
	5550 1950 5550 2200
Wire Wire Line
	5550 2200 4750 2200
Wire Wire Line
	4750 2200 4750 2400
Wire Wire Line
	4750 2400 4850 2400
Wire Wire Line
	5450 2500 5550 2500
Wire Wire Line
	5550 2500 5550 2250
Wire Wire Line
	5550 2250 4700 2250
Wire Wire Line
	4700 2250 4700 2050
Wire Wire Line
	4700 2050 4850 2050
Text GLabel 4650 1850 0    50   Input ~ 0
R
Wire Wire Line
	4650 1850 4850 1850
Text GLabel 4650 2600 0    50   Input ~ 0
S
Wire Wire Line
	4650 2600 4850 2600
Text GLabel 5750 1950 2    50   Input ~ 0
Q1
Wire Wire Line
	5750 1950 5550 1950
Connection ~ 5550 1950
Text GLabel 5750 2500 2    50   Input ~ 0
Q2
Wire Wire Line
	5750 2500 5550 2500
Connection ~ 5550 2500
Wire Wire Line
	5450 3400 5550 3400
Wire Wire Line
	5550 3400 5550 3650
Wire Wire Line
	5550 3650 4750 3650
Wire Wire Line
	4750 3650 4750 3850
Wire Wire Line
	4750 3850 4850 3850
Wire Wire Line
	5450 3950 5550 3950
Wire Wire Line
	5550 3950 5550 3700
Wire Wire Line
	5550 3700 4700 3700
Wire Wire Line
	4700 3700 4700 3500
Wire Wire Line
	4700 3500 4850 3500
Text GLabel 4650 3300 0    50   Input ~ 0
R
Wire Wire Line
	4650 3300 4850 3300
Text GLabel 4650 4050 0    50   Input ~ 0
S
Wire Wire Line
	4650 4050 4850 4050
Text GLabel 5750 3400 2    50   Input ~ 0
Q1
Wire Wire Line
	5750 3400 5550 3400
Connection ~ 5550 3400
Text GLabel 5750 3950 2    50   Input ~ 0
Q2
Wire Wire Line
	5750 3950 5550 3950
Connection ~ 5550 3950
$Comp
L 4xxx:4001 U2
U 1 1 6261404C
P 5150 3400
F 0 "U2" H 5150 3600 50  0000 C CNN
F 1 "4001" H 5150 3400 50  0000 C CNN
F 2 "" H 5150 3400 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4000bms-01bms-02bms-25bms.pdf" H 5150 3400 50  0001 C CNN
	1    5150 3400
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4001 U2
U 2 1 62616F24
P 5150 3950
F 0 "U2" H 5150 3750 50  0000 C CNN
F 1 "4001" H 5150 3950 50  0000 C CNN
F 2 "" H 5150 3950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4000bms-01bms-02bms-25bms.pdf" H 5150 3950 50  0001 C CNN
	2    5150 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
