# RAINWATER TANK

The purpose of this page is to explain the realization of the automated filling of a rainwater tank.

It uses some basic components :

 * a relay
 * a contactor
 * a PN2222 transistor
 * some resistors
 * a diode

The project named level-control is an automatic and connected level controller. It uses :

 * an ESP8266 WEMOS D1 MINI
 * a distance sensor : JSNSR-04T
 * a level sensor
 * 2 relays
 * a motorized valve

### ELECTRONICS

The schematics are made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2021/06/controle-de-niveau-deau.html

