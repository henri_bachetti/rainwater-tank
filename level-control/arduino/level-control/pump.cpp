
#include <Arduino.h>

#include "pump.h"

#define LOG_DOMAIN LOG_PUMP
#include "debug.h"

const char *pumpStateName[] = {"UNKNOWN", "ON", "OFF"};

Pump::Pump(void) :
  m_state(PUMP_UNKNOWN)
{
}

void Pump::setup(void)
{
  pinMode(PUMP_ON, OUTPUT);
  pinMode(PUMP_OFF, OUTPUT);
  off();
}

void Pump::on(void)
{
  log_printf(">>>> TURN ON PUMP (pump is %s)\n", pumpStateName[m_state]);
  if (m_state != PUMP_IS_ON) {
    log_println(">>>> TURN IT ON");
    digitalWrite(PUMP_ON, HIGH);
    delay(100);
    digitalWrite(PUMP_ON, LOW);
    m_state = PUMP_IS_ON;
  }
}

void Pump::off(void)
{
  log_printf(">>>> TURN OFF PUMP (pump is %s)\n", pumpStateName[m_state]);
  if (m_state != PUMP_IS_OFF) {
    log_println(">>>> TURN IT OFF");
    digitalWrite(PUMP_OFF, HIGH);
    delay(100);
    digitalWrite(PUMP_OFF, LOW);
    m_state = PUMP_IS_OFF;
  }
}
