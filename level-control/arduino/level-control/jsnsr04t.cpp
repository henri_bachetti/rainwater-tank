
#include <Arduino.h>
#include <NewPingESP8266.h>

#include "jsnsr04t.h"

#define LOG_DOMAIN LOG_JSNSR04T
#include "debug.h"

#define SOUND_VELOCITY 0.034
#define CM_TO_INCH 0.393701

NewPingESP8266 sonar(TRIG, ECHO, MAX_DISTANCE);

JsnSr04t::JsnSr04t(void)
{
}

void JsnSr04t::setup(void)
{
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);
}

float JsnSr04t::read(float min, float max)
{
  long duration;
  float distance;

  for (int n = 0 ; n < 10 ; n++) {
    distance = sonar.ping_cm();
    if (distance < min || distance > max) {
      log_printf("%f out of bounds\n", distance);
      delay(100);
    }
    else {
      log_printf("distance %.1f cm\n", distance);
      return distance;
    }
  }
  log_println("failed\n");
  return -1;
}
