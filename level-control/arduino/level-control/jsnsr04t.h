
#ifndef __JSNSR04T__
#define __JSNSR04T__

#define TRIG        13
#define ECHO        12
#define MAX_DISTANCE 200

class JsnSr04t {
public:
  JsnSr04t();
  void setup(void);
  float read(float min, float max);
};

#endif
