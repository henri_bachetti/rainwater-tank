
#include <Arduino.h>
#include <Ticker.h>

#include "valve.h"

#define LOG_DOMAIN LOG_VALVE
#include "debug.h"

Ticker timer;
const char *valveStateName[] = {"UNKNOWN", "CLOSED", "OPENING", "OPEN", "CLOSING"};

void changeState(Valve *v)
{
  if (v->state() == VALVE_IS_OPENING) {
    v->isOpen();
  }
  else if (v->state() == VALVE_IS_CLOSING) {
    v->isClosed();
  }
  timer.detach();
}

Valve::Valve(void) :
  m_state(VALVE_UNKNOWN)
{
}

void Valve::setup(void)
{
  pinMode(OPEN_PIN, OUTPUT);
  pinMode(CLOSE_PIN, OUTPUT);
  close();
}

void Valve::open(void)
{
  log_printf(">>>> OPEN VALVE (valve is %s)\n", valveStateName[m_state]);
  if (m_state == VALVE_IS_CLOSED) {
    log_println(">>>> OPEN IT");
    m_state = VALVE_IS_OPENING;
    digitalWrite(OPEN_PIN, HIGH);
    timer.attach(OPEN_TIME, changeState, this);
  }
}

void Valve::close(void)
{
  log_printf(">>>> CLOSE VALVE (valve is %s)\n", valveStateName[m_state]);
  if (m_state == VALVE_IS_OPEN || m_state == VALVE_UNKNOWN) {
    log_println(">>>> CLOSE IT");
    m_state = VALVE_IS_CLOSING;
    digitalWrite(CLOSE_PIN, HIGH);
    timer.attach(OPEN_TIME, changeState, this);
  }
}

void Valve::isOpen(void)
{
  log_println(">>>> VALVE is OPEN");
  m_state = VALVE_IS_OPEN;
  digitalWrite(OPEN_PIN, LOW);
}

void Valve::isClosed(void)
{
  log_println(">>>> VALVE is CLOSED");
  m_state = VALVE_IS_CLOSED;
  digitalWrite(CLOSE_PIN, HIGH);
}
