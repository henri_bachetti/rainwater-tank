
#ifndef __PUMP__
#define __PUMP__

#define PUMP_ON         15
#define PUMP_OFF        16

enum pumpState {PUMP_UNKNOWN, PUMP_IS_ON, PUMP_IS_OFF};
extern const char *pumpStateName[];

class Pump {
public:
  Pump();
  void setup(void);
  void on(void);
  void off(void);
  pumpState state(void) {return m_state;}
private:
  pumpState m_state;
};

#endif
