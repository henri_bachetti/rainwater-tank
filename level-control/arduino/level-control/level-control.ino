
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ESPAsyncWebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <FS.h>

#define LOG_DOMAIN LOG_MAIN
#include "debug.h"
#include "jsnsr04t.h"
#include "valve.h"
#include "pump.h"

// GPIOs
#define SENSOR          14

// customization
#define TANKS           3       // number of tanks
#define TOTAL_VOLUME    1530    // total tank volume
#define MAX_VOLUME      1400    // maximal tank admitted volume
#define MIN_VOLUME      200     // minimal tank volume
#define TOTAL_HEIGHT    100.0   // height of the tank in cm
#define TOP_RADIUS      45.0    // tank's top radius in cm
#define BOTTOM_RADIUS   35.0    // tank's bottom radius in cm
#define MIN_DISTANCE    22.0    // distance between tank's top level and JSNSR04T in cm. must be > minimal JSNSR04T distance

const char* ssid = "........";
const char* password = "........";

AsyncWebServer server(80);
const char* ntpServer = "pool.ntp.org";
time_t  bootTime;
char timeBuffer[80];
unsigned connections;

JsnSr04t jsnsr04t;
Valve valve;
Pump pump;
float remainingVolume;

String ntpUrl("pool.ntp.org");

void onStationConnected(const WiFiEventStationModeConnected& evt) {
  Serial.println("Connected to AP successfully!");
}

void onStationDisconnected(const WiFiEventStationModeDisconnected& evt) {
  Serial.print("Station disconnected: ");
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(evt.reason);
  Serial.printf("Trying to reconnect to %s\n", ssid);
  WiFi.reconnect();
}

void onStationGotIP(const WiFiEventStationModeGotIP& evt) {
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.printf("WiFi connected to %s\n", ssid);
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  connections++;
}

WiFiEventHandler stationConnectedHandler;
WiFiEventHandler stationDisconnectedHandler;
WiFiEventHandler stationGotIpHandler;

void handleNotFound(AsyncWebServerRequest * request) {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += request->url();
  message += "\nMethod: ";
  message += request->methodToString();
  message += "\nArguments: ";
  message += request->args();
  message += "\n";
  for (uint8_t i = 0; i < request->args(); i++) {
    message += " " + request->argName(i) + ": " + request->arg(i) + "\n";
  }
  request->send(404, "text/plain", message);
}

String templateProcessor(const String& var)
{
  if (var == "BOOT") {
    return timeBuffer;
  }
  if (var == "CONNECTIONS") {
    return String(connections);
  }
  if (var == "VOLUME") {
    return String(TOTAL_VOLUME);
  }
  if (var == "RSSI") {
    return String(WiFi.RSSI());
  }
  if (var == "REMAIN") {
    return String((int)remainingVolume);
  }
  if (var == "VALVE") {
    return valve.state() == VALVE_IS_OPEN || valve.state() == VALVE_IS_OPENING ? "/open.jpg" : "/closed.jpg";
  }
  return "";
}

void setup(void)
{
  Serial.begin(115200);
  log_println("");
  pinMode(SENSOR, INPUT_PULLUP);
  pump.setup();
  jsnsr04t.setup();
  valve.setup();
  SPIFFS.begin();
  stationConnectedHandler = WiFi.onStationModeConnected(onStationConnected);
  stationDisconnectedHandler = WiFi.onStationModeDisconnected(onStationDisconnected);
  stationGotIpHandler = WiFi.onStationModeGotIP(onStationGotIP);
  configTzTime("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00", ntpServer);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.begin(ssid, password);
  log_print("\nConnecting to "); log_println(ssid);
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html", "text/html", false, templateProcessor);
  });
  server.on("/gauge.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    log_println("serving gauge.js");
    if (!SPIFFS.exists("/gauge.js")) {
      log_println("gauge.js not found");
    }
    request->send(SPIFFS, "/gauge.js", "text/javascript");
  });
  server.on("/open.jpg", HTTP_GET, [](AsyncWebServerRequest * request) {
    log_println("serving open.jpg");
    request->send(SPIFFS, "/open.jpg", "image/png");
  });
  server.on("/closed.jpg", HTTP_GET, [](AsyncWebServerRequest * request) {
    log_println("serving closed.jpg");
    request->send(SPIFFS, "/closed.jpg", "image/png");
  });
  server.onNotFound(handleNotFound);
  server.begin();
  log_println("HTTP server started");
}

float getVolume(float distance)
{
  float totalHeight = TOTAL_HEIGHT;
  float r1 = BOTTOM_RADIUS;
  float r2 = TOP_RADIUS;
  float height = totalHeight - distance + MIN_DISTANCE;
  log_printf("height %.1f cm\n", height);
  float r = r1 + (r2 - r1) * (height / totalHeight);
  log_printf("radius %.1f cm\n", r);
  float remain = height * 3.14 / 3 * (pow(r1, 2) + pow(r2, 2) + r1 * r2);
  return remain / 1000 * TANKS;
}

void loop(void)
{
  static bool justBooted = true;
  static unsigned long lastTime;
  unsigned long tick = millis();

  if (millis() - lastTime >= 1000) {
    if (bootTime < 1000) {
      bootTime = time(NULL);
      struct tm *pTime = localtime(&bootTime);
      strftime(timeBuffer, 80, "%d/%m/%Y %H:%M", pTime);
    }
  }
  if (justBooted || tick - lastTime > 5000) {
    lastTime = tick;
    justBooted = false;
    float distance;
    if (digitalRead(SENSOR) == LOW) {
      log_println("SENSOR is ON");
      distance = MIN_DISTANCE;
    }
    else {
      log_println("level control");
      distance = jsnsr04t.read(5, 150);
      if (distance == -1) {
        valve.close();
        pump.off();
        return;
      }
    }
    remainingVolume = getVolume(distance);
    log_printf("remaining volume %d\n", (int)remainingVolume);
    if (remainingVolume >= MAX_VOLUME) {
      valve.close();
    }
    else if (remainingVolume < MAX_VOLUME * 0.95) {
      valve.open();
    }
    if (remainingVolume >= MIN_VOLUME * 1.05) {
      pump.on();
    }
    else if (remainingVolume < MIN_VOLUME) {
      pump.off();
    }
  }
}
