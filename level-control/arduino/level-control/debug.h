
#ifndef _DEBUG_H_
#define _DEBUG_H_

#define LOG_MAIN        0x01
#define LOG_JSNSR04T    0x02
#define LOG_VALVE       0x04
#define LOG_PUMP        0x08

#define LOGS_ACTIVE     (LOG_MAIN | LOG_JSNSR04T | LOG_VALVE | LOG_PUMP)

#if (LOGS_ACTIVE & LOG_DOMAIN)
#define log_print       Serial.print
#define log_println     Serial.println
#define log_printf      Serial.printf
#else
#define log_print
#define log_println
#define log_printf
#endif

#endif
