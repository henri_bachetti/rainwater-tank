
#ifndef __VALVE__
#define __VALVE__

#define OPEN_PIN          4
#define CLOSE_PIN         5
#define OPEN_TIME         20

enum valveState {VALVE_UNKNOWN, VALVE_IS_CLOSED, VALVE_IS_OPENING, VALVE_IS_OPEN, VALVE_IS_CLOSING};
extern const char *valveStateName[];

class Valve {
public:
  Valve();
  void setup(void);
  void open(void);
  void close(void);
  void isOpen(void);
  void isClosed(void);
  valveState state(void) {return m_state;}
private:
  valveState m_state;
};

#endif
