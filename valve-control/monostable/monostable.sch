EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4xxx:4001 U1
U 1 1 625B6D49
P 3950 2850
F 0 "U1" H 3950 3175 50  0000 C CNN
F 1 "4001" H 3950 3084 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_LongPads" H 3950 2850 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4000bms-01bms-02bms-25bms.pdf" H 3950 2850 50  0001 C CNN
	1    3950 2850
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4001 U1
U 2 1 625B8258
P 5150 2950
F 0 "U1" H 5150 3275 50  0000 C CNN
F 1 "4001" H 5150 3184 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_LongPads" H 5150 2950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4000bms-01bms-02bms-25bms.pdf" H 5150 2950 50  0001 C CNN
	2    5150 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 625C26D4
P 4500 2850
F 0 "C1" V 4245 2850 50  0000 C CNN
F 1 "100µF" V 4336 2850 50  0000 C CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 4538 2700 50  0001 C CNN
F 3 "~" H 4500 2850 50  0001 C CNN
	1    4500 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 2850 4250 2850
Wire Wire Line
	4850 2850 4750 2850
Wire Wire Line
	4750 2850 4750 3050
Wire Wire Line
	4750 3050 4850 3050
Wire Wire Line
	5450 2950 5550 2950
Wire Wire Line
	5550 2950 5550 3200
Wire Wire Line
	5550 3200 3550 3200
Wire Wire Line
	3550 3200 3550 2950
Wire Wire Line
	3550 2950 3650 2950
Wire Wire Line
	4650 2850 4750 2850
Connection ~ 4750 2850
$Comp
L Device:R R1
U 1 1 625DE3A7
P 4750 2600
F 0 "R1" H 4820 2646 50  0000 L CNN
F 1 "100K" H 4820 2555 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4680 2600 50  0001 C CNN
F 3 "~" H 4750 2600 50  0001 C CNN
	1    4750 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2750 4750 2850
Wire Wire Line
	4750 2350 4750 2450
Wire Wire Line
	3500 2750 3650 2750
$Comp
L power:+5V #PWR0103
U 1 1 627BC462
P 4750 2350
F 0 "#PWR0103" H 4750 2200 50  0001 C CNN
F 1 "+5V" H 4765 2523 50  0000 C CNN
F 2 "" H 4750 2350 50  0001 C CNN
F 3 "" H 4750 2350 50  0001 C CNN
	1    4750 2350
	1    0    0    -1  
$EndComp
Text GLabel 3500 2750 0    50   Input ~ 0
IN1
Text GLabel 5650 2950 2    50   Input ~ 0
OUT1
Wire Wire Line
	5650 2950 5550 2950
Connection ~ 5550 2950
Wire Notes Line
	3000 2650 3100 2650
Wire Notes Line
	3100 2650 3100 2150
Wire Notes Line
	3100 2150 3200 2150
Wire Notes Line
	3200 2150 3200 2650
Wire Notes Line
	3200 2650 3300 2650
Wire Notes Line
	5850 2650 6000 2650
Wire Notes Line
	6000 2650 6000 2150
Wire Notes Line
	6000 2150 7000 2150
Wire Notes Line
	7000 2150 7000 2650
Wire Notes Line
	7000 2650 7100 2650
Wire Notes Line
	7000 3500 7100 3500
Wire Notes Line
	7000 4000 7000 3500
Wire Notes Line
	6000 4000 7000 4000
Wire Notes Line
	6000 3500 6000 4000
Wire Notes Line
	5850 3500 6000 3500
Wire Notes Line
	3200 3500 3300 3500
Wire Notes Line
	3200 4000 3200 3500
Wire Notes Line
	3100 4000 3200 4000
Wire Notes Line
	3100 3500 3100 4000
Wire Notes Line
	3000 3500 3100 3500
Text GLabel 5650 4000 2    50   Input ~ 0
OUT2
Text GLabel 3500 4100 0    50   Input ~ 0
IN2
Wire Wire Line
	5650 4000 5550 4000
Connection ~ 5550 4000
Wire Wire Line
	4750 4500 4750 4600
$Comp
L power:GND #PWR0104
U 1 1 625EFB0B
P 4750 4600
F 0 "#PWR0104" H 4750 4350 50  0001 C CNN
F 1 "GND" H 4755 4427 50  0000 C CNN
F 2 "" H 4750 4600 50  0001 C CNN
F 3 "" H 4750 4600 50  0001 C CNN
	1    4750 4600
	1    0    0    -1  
$EndComp
Connection ~ 4750 4100
Wire Wire Line
	4750 4100 4750 4200
$Comp
L Device:R R2
U 1 1 625EEBD0
P 4750 4350
F 0 "R2" H 4820 4396 50  0000 L CNN
F 1 "100K" H 4820 4305 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4680 4350 50  0001 C CNN
F 3 "~" H 4750 4350 50  0001 C CNN
	1    4750 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4000 4250 4000
Wire Wire Line
	4750 4000 4750 4100
Connection ~ 4750 4000
Wire Wire Line
	4650 4000 4750 4000
$Comp
L Device:CP C2
U 1 1 625E3CBE
P 4500 4000
F 0 "C2" V 4250 4000 50  0000 C CNN
F 1 "100µF" V 4350 4000 50  0000 C CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 4538 3850 50  0001 C CNN
F 3 "~" H 4500 4000 50  0001 C CNN
	1    4500 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4750 4100 4850 4100
Wire Wire Line
	4750 3900 4750 4000
Wire Wire Line
	4850 3900 4750 3900
Wire Wire Line
	3500 4100 3650 4100
Wire Wire Line
	3550 3900 3650 3900
Wire Wire Line
	3550 3750 3550 3900
Wire Wire Line
	5550 3750 3550 3750
Wire Wire Line
	5550 4000 5550 3750
Wire Wire Line
	5450 4000 5550 4000
$Comp
L 4xxx:4011 U2
U 2 1 625BB0BE
P 5150 4000
F 0 "U2" H 5150 3650 50  0000 C CNN
F 1 "4011" H 5150 3750 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_LongPads" H 5150 4000 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 5150 4000 50  0001 C CNN
	2    5150 4000
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4011 U2
U 1 1 625B9B05
P 3950 4000
F 0 "U2" H 3950 3650 50  0000 C CNN
F 1 "4011" H 3950 3750 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_LongPads" H 3950 4000 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 3950 4000 50  0001 C CNN
	1    3950 4000
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4001 U1
U 1 1 626135B8
P 4200 5600
F 0 "U1" H 4200 5925 50  0000 C CNN
F 1 "4001" H 4200 5834 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_LongPads" H 4200 5600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4000bms-01bms-02bms-25bms.pdf" H 4200 5600 50  0001 C CNN
	1    4200 5600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4011 U2
U 1 1 6261621A
P 4200 6150
F 0 "U2" H 4200 5800 50  0000 C CNN
F 1 "4011" H 4200 5900 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_LongPads" H 4200 6150 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 4200 6150 50  0001 C CNN
	1    4200 6150
	1    0    0    -1  
$EndComp
Text GLabel 3700 5600 0    50   Input ~ 0
IN
Wire Wire Line
	3700 5600 3800 5600
Wire Wire Line
	3800 5600 3800 5500
Wire Wire Line
	3800 5500 3900 5500
Wire Wire Line
	3800 5600 3800 5700
Wire Wire Line
	3800 5700 3900 5700
Connection ~ 3800 5600
Text GLabel 3700 6150 0    50   Input ~ 0
IN
Wire Wire Line
	3900 6050 3800 6050
Wire Wire Line
	3800 6050 3800 6150
Wire Wire Line
	3800 6150 3700 6150
Wire Wire Line
	3800 6150 3800 6250
Wire Wire Line
	3800 6250 3900 6250
Connection ~ 3800 6150
Text GLabel 4600 5600 2    50   Input ~ 0
OUT
Text GLabel 4600 6150 2    50   Input ~ 0
OUT
Wire Wire Line
	4600 6150 4500 6150
Wire Wire Line
	4600 5600 4500 5600
$EndSCHEMATC
